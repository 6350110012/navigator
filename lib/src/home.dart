import 'package:flutter/material.dart';
import 'package:flutter_login/src/login.dart';
import 'package:flutter_login/src/signup.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding:const EdgeInsets.only(top: 40),
                child: Text(
                    "WELCOME",
                  style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.purple
                  ),
                ),
              ),

              const Padding(
                padding: EdgeInsets.only(top: 10,right: 40,left: 40),
                child: Icon(Icons.language_outlined,color: Colors.purple,)
              ),
              Image.asset("assets/images/bear.jpg"),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 230,
                  height: 40,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.purple),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(18.0),
                            )
                        )
                    ),
                    child: Text("SIGN IN",style: TextStyle(fontSize: 18),),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context){
                            return LoginPage(title: 'page1');
                          })
                      );
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  width: 230,
                  height: 40,
                  child: ElevatedButton(
                    style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.purpleAccent),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16.0),
                            )
                        )
                    ),
                    child: Text("SIGN UP",style: TextStyle(fontSize: 18),),
                    onPressed: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context){
                            return RegisterPage(title: 'page2');
                        })
                      );
                    },
                  ),
                ),
              )
            ],
          ),
      ),
    );
  }
}
